from django.http import HttpResponse
from django.conf import settings
from middleware_api import models

class ErrorHandlerMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_exception(self, request, exception):
        msg = exception
        obj = {"status": "couldn't get",
               "error":msg
                }
        error_item = models.ErrorItems(error = obj)
        error_item.save()
        return HttpResponse(msg)
