from django.db import models

class ErrorItems(models.Model):

    error = models.CharField(max_length=300)

    def __str__(self):
        return self.error
