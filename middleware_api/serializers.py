from rest_framework import serializers
from error_api.models import ErrorItems


class ErrorItemsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ErrorItems
        fields = ('id',
                  'error',
                  'created_on'
                 )
