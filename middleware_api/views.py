from django.shortcuts import render, HttpResponse
from rest_framework.exceptions import APIException

def error1(request):

    x=8/0
    return HttpResponse("You are not gonna get this message from views.py")

def error2(request):
    obj= {"a":200}
    a = "abc"
    b= a+obj
    return APIException("You are not gonna get this message from views.py")
