1-This project allows you to enter the error message to log into postgresql.
2- File will automatically input the  error msg in Json format in the psql database.
3- Credentials of the database is provided in the settings.py and can be changed.
4- Files has two sample error generating URLs:
   http://localhost/8000/error1---- Generates a non divisible by zero error
   http://localhost/8000/error2---- Generates a concatenation error for a string and a json object

5-This project utilises ErrorItems model to store data into the database.
6- ErrorItemsSerializer is used to serialize data before sending the data into the db.
7-ErrorHandlerMiddleware is used to handle the error response in the api and log the error message to the database
